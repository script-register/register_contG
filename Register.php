<?php

// Notificar solamente errores de ejecución
error_reporting(E_ERROR | E_WARNING | E_PARSE);

// Notificar E_NOTICE también puede ser bueno (para informar de variables
// no inicializadas o capturar errores en nombres de variables ...)
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);

// Notificar todos los errores excepto E_NOTICE
error_reporting(E_ALL ^ E_NOTICE);

// Notificar todos los errores de PHP (ver el registro de cambios)
error_reporting(E_ALL);

// Notificar todos los errores de PHP
error_reporting(-1);

// Lo mismo que error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of register
 *
 * @author ivanzarco
 */
include "connection/Connection.php";

class Register {

    //put your code here

    public function register() {

        $db = new Connection();
        $conn = $db->connectionOpen();
        //$db->connectionClose($conn);
        //$dbGtm = new Connection();
        $connGtm = $db->connectionOpenGtm();

        /* Categorias
         * hs => Historias
         * ac => Arte y Cultura
         * pr => Personajes
         * ly => Leyenda
         * re => Gastronomía
         * rq => Zonas arqueológicas
         * ev => Celebraciones y festivales
         * at => Sitios de interés
         * na => Naturaleza
         */




        /* $sql = "SELECT * FROM category";
          $result = $conn->query($sql); */
        //$rows = $result->fetch_assoc();

        $countRow = "SELECT count(*) AS total FROM _blog WHERE activo = 1";
        $num = $connGtm->query($countRow);
        $numTotal = $num->fetch_assoc();
        $countTotal = $numTotal["total"];

        $sqlGtm = "SELECT * FROM _blog WHERE activo = 1 ORDER BY reg_fecha ASC";
        $resultGtm = $connGtm->query($sqlGtm);

        //$num = $connGtm->mysqli_num_rows($resultGtm);
        $db->connCloseGtm($connGtm);

        //$register = $insertConGrl()
        //$resultArray = [];
        //$resultInsert = "";
        $success = "ok";
        //$danger = "error";
        //$insertConGrl = "";
        //$last_id = "";
        $explodeString = "";

        while ($row = $resultGtm->fetch_assoc()) {

            $cont = $row["texto"];
            $title = $row["titulo"];
            $description = $row["intro"];
            $stateId = $row["estado"];
            $categoryId = $row["_es"];

            //$states = 'a:32:{i:0;s:1:"1";i:1;s:1:"2";i:2;s:1:"3";i:3;s:1:"4";i:4;s:1:"5";i:5;s:1:"6";i:6;s:1:"7";i:7;s:1:"8";i:8;s:1:"9";i:9;s:2:"10";i:10;s:2:"11";i:11;s:2:"12";i:12;s:2:"13";i:13;s:2:"14";i:14;s:2:"15";i:15;s:2:"16";i:16;s:2:"17";i:17;s:2:"18";i:18;s:2:"19";i:19;s:2:"20";i:20;s:2:"21";i:21;s:2:"22";i:22;s:2:"23";i:23;s:2:"24";i:24;s:2:"25";i:25;s:2:"26";i:26;s:2:"27";i:27;s:2:"28";i:28;s:2:"29";i:29;s:2:"30";i:30;s:2:"31";i:31;s:2:"32";}';
            //$two = 'a:1:{i:0;s:2:"30";}';


            /* Seccion de separación de categorias */
            $aCategory = explode('a:', $categoryId);
            $bCategory = explode(':{', $aCategory[1]);

            $explodeCategory = explode(':"', $categoryId);

            $explodetwoCategory = [];
            $arrayCategory = [];
            for ($x = 1; $x <= $bCategory[0]; $x++) {
                $explodetwoCategory[] = explode('";', $explodeCategory[$x]);
            }

            for ($xx = 0; $xx < $bCategory[0]; $xx++) {
                $arrayCategory[] = $explodetwoCategory[$xx][0];
            }

            /* Seccion de separación de estados */
            $aState = explode('a:', $stateId);
            $bState = explode(':{', $aState[1]);

            $explodeString = explode(':"', $stateId);

            $explodetwo = [];
            $array = [];
            for ($x = 1; $x <= $bState[0]; $x++) {
                $explodetwo[] = explode('";', $explodeString[$x]);
            }

            for ($xx = 0; $xx < $bState[0]; $xx++) {
                $array[] = $explodetwo[$xx][0];
            }


            //return $datas;
            //$stateArray = $jsonStates;


            $exists = "SELECT COUNT(*) AS total FROM content_general";
            $result = $conn->query($exists);
            $rowExists = $result->fetch_assoc();
            $total = $rowExists["total"];

            //$db->connectionClose($conn);

            if ($total < $countTotal) {

                $insertConGrl = "INSERT INTO content_general (content, title, page_clic, image, status_id, description, user_id) VALUES ('$cont','$title',null,null,1,'$description',1)";
                $conn->query($insertConGrl);

                $last_id = $conn->insert_id;

                foreach ($array as $item => $value) {
                    $insertGrlRelation = "INSERT INTO relation_content_general (content_general_id, state_id) VALUES ('$last_id','$value')";
                    $conn->query($insertGrlRelation);

                    $last_id_contg = $conn->insert_id;

                    foreach ($arrayCategory as $itemCatg => $valueCateg) {
                        if ($valueCateg == 'at') {
                            $insertGrlCategory = "INSERT INTO relation_category_content (relation_content_general_id, category_id, category_sub_id) VALUES ('$last_id_contg', '2', null)";
                            $conn->query($insertGrlCategory);
                        } elseif ($valueCateg == 'hs') {
                            $insertGrlCategory = "INSERT INTO relation_category_content (relation_content_general_id, category_id, category_sub_id) VALUES ('$last_id_contg', '3', null)";
                            $conn->query($insertGrlCategory);
                        } elseif ($valueCateg == 'pr') {
                            $insertGrlCategory = "INSERT INTO relation_category_content (relation_content_general_id, category_id, category_sub_id) VALUES ('$last_id_contg', '4', null)";
                            $conn->query($insertGrlCategory);
                        } elseif ($valueCateg == 'ly') {
                            $insertGrlCategory = "INSERT INTO relation_category_content (relation_content_general_id, category_id, category_sub_id) VALUES ('$last_id_contg', '5', null)";
                            $conn->query($insertGrlCategory);
                        } elseif ($valueCateg == 're') {
                            $insertGrlCategory = "INSERT INTO relation_category_content (relation_content_general_id, category_id, category_sub_id) VALUES ('$last_id_contg', '6', null)";
                            $conn->query($insertGrlCategory);
                        } elseif ($valueCateg == 'ev') {
                            $insertGrlCategory = "INSERT INTO relation_category_content (relation_content_general_id, category_id, category_sub_id) VALUES ('$last_id_contg', '7', null)";
                            $conn->query($insertGrlCategory);
                        } elseif ($valueCateg == 'rq') {
                            $insertGrlCategory = "INSERT INTO relation_category_content (relation_content_general_id, category_id, category_sub_id) VALUES ('$last_id_contg', '8', null)";
                            $conn->query($insertGrlCategory);
                        } elseif ($valueCateg == 'ac') {
                            $insertGrlCategory = "INSERT INTO relation_category_content (relation_content_general_id, category_id, category_sub_id) VALUES ('$last_id_contg', '9', null)";
                            $conn->query($insertGrlCategory);
                        } elseif ($valueCateg == 'na') {
                            $insertGrlCategory = "INSERT INTO relation_category_content (relation_content_general_id, category_id, category_sub_id) VALUES ('$last_id_contg', '10', null)";
                            $conn->query($insertGrlCategory);
                        }
                    }
                }
            }


            //$last_id = $conn->insert_id;
            //$insertGrlRelation = "INSERT INTO relation_content_general (content_general_id, state_id) VALUES ('$last_id','1')";
            //$insertGrlCategory = "INSERT INTO relation_category_content (relation_content_general_id, category_id, category_sub_id) VALUES ('$last_id', '1', null)";
        }
        //return $explodeString[1];
        //return $stateArray;
        //$db->connectionClose($conn);
        return $success;
        //$num = $connGtm->mysqli_num_rows($resultGtm);
        //return $resultInsert;
        //$dbGtm->connCloseGtm($connGtm);
    }

}
