<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of connection
 *
 * @author ivanzarco
 */
class Connection {

    public function connectionOpen() {
        $servername = "localhost";
        $username = "root";
        $password = "root";
        $dbName = 'db_gtmProduction';
        $dbName02 = 'gmtpro';

// Create connection
        $conn = new mysqli($servername, $username, $password, $dbName);

        $conno2 = new mysqli($servername, $username, $password, $dbName02);

// Check connection
        if ($conn->connect_error || $conno2->connect_error) {
            die("Connection failed 01: " . $conn->connect_error . " Connection failed 02: " . $conno2->connect_error);
        }
        echo "Connected successfully";

        return $conn;
    }

    public function connectionOpenGtm() {
        $servername = "localhost";
        $username = "root";
        $password = "root";
        $dbName = 'gmtpro';

// Create connection
        $conn = new mysqli($servername, $username, $password, $dbName);


// Check connection
        if ($conn->connect_error) {
            die("Connection failed 01: " . $conn->connect_error);
        }
        echo "Connected successfullyGtm";

        return $conn;
    }

    public function connCloseGtm($conn) {
        $conn->close();
    }

    public function connectionClose($conn) {
        $conn->close();
    }

}
